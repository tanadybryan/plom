.. Plom documentation
   Copyright (C) 2020 Andrew Rechnitzer
   Copyright (C) 2022-2024 Colin B. Macdonald
   SPDX-License-Identifier: AGPL-3.0-or-later


Identifying papers
==================

At some point the Plom system needs to know which paper belongs to which student and this can be done in several ways:

1. Prenamed papers: Plom can produce papers with student names and IDs
   already printed on them.
2. Automated ID reading — When tests are producing using Plom’s ID
   Template, the system can use `machine learning <https://xkcd.com/1838>`_
   to read the digits from the student-ID boxes and match against the
   classlist.
   In practice these appear to be over 95% accurate, but are not
   infallible.
3. Manual association — The simplest method is for a human to just read
   the ID from the page and enter it into the system.

All these eventually require verification by a human.


Running the auto-identifier
---------------------------

This will only work if your assessment uses our ``idBox`` template because
the code first looks for the outer bold rectangle and then tries to locate
the 8 digits of the student number, based on known locations inside that
larger box.

To run the auto-identifier, locate it under "ID Progress" in the Plom
web interface.

..
    TODO: xref to the `plom_server.Identify` app later, assuming those
    top-level apps show up in the docs in a meaningful way.
    I don't really want these docs to describe exactly what to click on
    the webpage b/c I'd prefer the webpage be self-documenting.

Note that by default a human will still need to confirm the
machine-read predictions.

.. tip::
   You can re-run the ID predictor at anytime, such as after confirming
   some of the IDs manually.


Running the auto-identifier (legacy server)
-------------------------------------------

1. Open the :doc:`Legacy Manager tool <manage>`, then "Progress" → "ID progress".
2. Optionally, adjust the top/bottom crop values, either manually or by
   clicking "Select interactively".  Only the vertical coordinate are used.
   The defaults probably work in most cases.
3. Click "Recognize digits in IDs" which starts a background job.
   Click "Refresh" to update the output window.
4. Click "Run matching tools".  This currently blocks and might take a
   few seconds (say 3 seconds for 1000 papers).
5. Click "Refresh prediction list" to update the table view.

.. caution::

   You should manually and carefully check the results (the Identifier client
   will show you these by default) because it does make mistakes, especially
   when there are additional names available in your classlist who did not
   write the test.


Manually identifying
--------------------

This is typically quite quick compared to marking and you will not need
to assign much person-time.
Since it does not require any heavy thinking it can be a good task for:

- the instructor-in-charge who is regularly interrupted by questions about papers,
- a (reliable) marker who finishes their other tasks early, or
- the scanner once they have finished scanning and uploading papers.

For now see https://plomgrading.org/docs/clientUse/identifying.html
